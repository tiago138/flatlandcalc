import PySimpleGUI as sg
import flatlandcalc


def main_inter():
    sg.theme('Dark')
    row1 = [sg.Button('Calcular Área'), sg.Button('Calcular Volume')]

    layout = [row1]

    window = sg.Window('Flatland Calc', layout)

    window.read()

    while True:
        event, values = window.read()
        if event == sg.WINDOW_CLOSED:
            break
        if event == 'Calcular Área':
            window.close()
        if event == 'Calcular Área':
            calc_area_inter()
        if event == 'Calcular Volume':
            window.close()
        if event == 'Calcular Volume':
            calc_vol_inter()

    window.close()


def calc_area_inter():
    row1 = [sg.Text('Profundidade'), sg.Input(key='profundidade')]
    row2 = [sg.Text('Largura'), sg.Input(key='largura')]
    row3 = [sg.Button('Calcular'), sg.Button('Voltar')]
    row4 = [sg.Text('Resultado'), sg.Text(key='resultado')]

    layout = [row1, row2, row3, row4]

    window = sg.Window('Flatland Calc Área', layout)
    window.read()

    while True:
        event, values = window.read()
        if event == sg.WINDOW_CLOSED:
            break
        if event == 'Voltar':
            window.close()
        if event == 'Voltar':
            main_inter()
        if event == 'Calcular':
            profundidade = float(values['profundidade'])
            largura = float(values['largura'])
            resultado = flatlandcalc.calc_area(profundidade, largura)
            window['resultado'].update(resultado)

    window.close()


def calc_vol_inter():
    row1 = [sg.Text('Profundidade'), sg.Input(key='profundidade')]
    row2 = [sg.Text('Largura'), sg.Input(key='largura')]
    row3 = [sg.Text('Altura'), sg.Input(key='altura')]
    row4 = [sg.Button('Calcular'), sg.Button('Voltar')]
    row5 = [sg.Text('Resultado'), sg.Text(key='resultado')]

    layout = [row1, row2, row3, row4, row5]

    window = sg.Window('Flatland Calc Volume', layout)
    window.read()

    while True:
        event, values = window.read()
        if event == sg.WINDOW_CLOSED:
            break
        if event == 'Voltar':
            window.close()
        if event == 'Voltar':
            main_inter()
        if event == 'Calcular':
            profundidade = float(values['profundidade'])
            largura = float(values['largura'])
            altura = float(values['altura'])
            resultado = flatlandcalc.calc_vol(profundidade, largura, altura)
            window['resultado'].update(resultado)

    window.close()


main_inter()
